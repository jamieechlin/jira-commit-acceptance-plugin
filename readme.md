Commit Acceptance plugin for JIRA
=================================

This software is distributed under the terms of the FSF Lesser Gnu
Public License.

Visit for latest info: http://confluence.atlassian.com/display/DEVNET/Commit+Acceptance+Plugin

For detailed information about the project, technical details or
installation, please read this wiki page.



JIRA: http://www.atlassian.com/software/jira/
(JIRA is bug tracking, issue tracking & project management software.)

Subversion: http://subversion.tigris.org/
(The goal of the Subversion project is to build a version control system that is a compelling replacement for CVS in the open source community.)

CVS: http://www.nongnu.org/cvs/
(CVS is a version control system, an important component of Source Configuration Management (SCM).)


Installation
------------

Build and Install as per a normal plugins-2 plugin.

Differences from original version
---------------------------------

There are a few main drivers behind this fork:

* Conversion to plugins 2 system for manageability, ease of development and testing
* Remove any per-repo configuration from the triggers themselves for manageability. Applying the commit acceptance criteria to a repo should be as simple as symlinking the trigger file.
* Allow greater flexibility in specifying which issues are allowed to be committed against through JQL, rather than a host of checkboxes.
* The assumption is this is run in a highly-managed environment, where there is one subversion repo per jira project, although this need not be the case.



Usage
-----

When the trigger is in place, any commit will invoke a JIRA REST endpoint. It will send the current committer name, rule set to use, repo name, and commit message.

The rule set is usually defined as '*', which means use the _Global_ configuration. The trigger can either be changed to default to the same name as the repo, or you could hard-code a rule set name.

Each issue key mentioned in the commit message is compared with the results from the configured JQL query. If all, or at least one key matches (depending on configuration) the commit is allowed to proceed.

A sample query might be:

    assignee = currentUser() and project = $repoName and status = 'In Progress' and issuetype not in (Epic, Story)

To break this down...

* each issue key mentioned must be assigned to the current committer (the query runs in the context of the user making the commit).
* the project name must be the same as the repo name
* the issue must be in the _In Progress_ state
* you cannot commit code against Epics and Stories

The only substitution available is $repoName, which will take the name of the repository the user is committing against.

Notes
-----

Only the python trigger (/client/python/svn/jira-client.py) has had the necessary changes made.
 
There is no longer a requirement for the trigger to log in with admin credentials. Hard-coding user names and passwords in trigger files is a greater security risk than the new one introduced, which is that conceivably someone could detect whether a given issue key matched the given criteria, should they know what the JQL query is.
  
There is a bug, in that an exception is thrown by the plugin on startup. Somehow the autowiring is not fully working, although it does not seem to affect its operation.
  
